import datetime
import re

import numpy as np
import scipy.sparse as sp
import xlrd
from scipy.sparse.linalg import svds

pattern = re.compile("(\w[\w']*\w|\w)")

stop_words = {''}

t1 = datetime.datetime.now()

book = xlrd.open_workbook('dataset_master.xls')

item_count = book.sheet_by_index(0).nrows - 1

k = 5
nresult = 5

data = []


def get_data(b):
    sh = b.sheet_by_index(0)
    for r in range(1, sh.nrows, 1):
        data.append((sh.cell_value(r, 0), sh.cell_value(r, 1)))


get_data(book)
num_terms = 1

titles = dict()
lexicon = dict()
rev_titles = dict()

for idx, item in enumerate(data):
    con = item[0].split()
    titles[item[1]] = idx
    rev_titles[idx] = item[1]

    for word in con:
        w = word.lower()
        if w in stop_words:
            continue
        elif w in lexicon:
            continue
        else:
            lexicon[w] = num_terms
            num_terms += 1

print(lexicon)
num_terms -= 1
lex_dict = {v: k for k, v in lexicon.items()}
print('lexicon dict formed')
print("number distinct words : %d" % num_terms)
t2 = datetime.datetime.now()
t1diff = t2 - t1
print("time: %s" % t1diff)

row = []
col = []
freq = []


def tf(ii):
    doc = data[ii]
    c = doc[0].lower().split()
    tf_dic = dict()

    for ww in c:
        if ww in stop_words:
            continue
        elif ww in tf_dic:
            tf_dic[ww] += 1.0
        else:
            tf_dic[ww] = 1.0

    for k, v in tf_dic.items():
        row.append(lexicon[k] - 1)
        col.append(ii)
        freq.append(v)


print("calculating term doc matrix")

for i in range(0, item_count):
    tf(i)

print("read in col and row form")
t3 = datetime.datetime.now()
t3diff = t3 - t2
print("time: %s " % t3diff)

print("converting to sparse representation")
stdm = sp.csc_matrix((freq, (row, col)), shape=(num_terms, item_count))
t4 = datetime.datetime.now()
t4diff = t4 - t3
print("time: %s" % t4diff)
print("converted to sparse representation")

print("calculating svds")
u, s, vt = svds(stdm, k=k, which='LM')
v = vt.T

threshold = 0.000000000000001
sinv = []
for t in s:
    if t < threshold:
        sinv.append(0.0)
    else:
        sinv.append(1.0 / t)

sinv = np.array(sinv)
sinv1 = np.diag(sinv)

s1 = np.diag(s)
us = np.dot(u, s1)
vs = np.dot(v, s1)

t5 = datetime.datetime.now()
t5diff = t5 - t4
print("time: %s" % t5diff)
print("calculated svds")


def simi_calc(t, word_dict, mat):
    idx = word_dict[t]
    similarity = []
    d1 = mat[idx - 1, :]
    n1 = np.linalg.norm(d1)
    for i, r in enumerate(mat):
        val = np.dot(d1, r) / (np.linalg.norm(r) * n1)
        similarity.append((val, i + 1))
    similarity.sort(key=lambda x: -x[0])
    return similarity


print("### query similiarity ###")
queries = open('queries', 'r').read().splitlines()

fout = open('query_result', 'w')

tmp = np.dot(u, sinv1)  # u*sigmainv
for query in queries:
    tf_vector = [0] * len(lexicon)
    query = query.lower()
    terms = pattern.split(query)
    for t in terms:
        t = t.lower()
        if t in lexicon:
            tf_vector[lexicon[t] - 1] += 1

    d1 = np.dot(tf_vector, tmp)
    n1 = np.linalg.norm(d1)
    similarity = []

    for i, r in enumerate(vs):
        # print(r)
        # print(i)
        val = np.dot(r, d1) / (np.linalg.norm(r) * n1)
        similarity.append((val, i + 1))
    similarity.sort(key=lambda x: -x[0])

    first = True
    print("query %s" % query)
    for val, idx in similarity[:nresult]:
        print("similiarity %f dokumen %s" % (val, rev_titles[idx]))
        if first:
            first = False
            fout.write(rev_titles[idx])
            continue
        fout.write(';\t' + rev_titles[idx])
    fout.write('\n')
fout.close()
